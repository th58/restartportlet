#
# restartportlet.py (created April 22, 2015 by th58)
# This python program provides an automated way of using the Tomcat Manager to restart a portlet without having to open a web page in a browser.
# The constants below must be configured before use.
#

import requests
import sys
import re
import os
from ConfigParser import SafeConfigParser

#
# Make sure the config file exists
#
if not os.path.isfile("config.ini"):
    print "Error: config.ini not found. Please copy config.ini.template to config.ini and fill it in with correct values"
    sys.exit()

#
# Make sure an environment is specified (eg "dev" or "test")
#
if len(sys.argv) < 2:
    print "Error: must specify section from config.ini (eg \"dev\" or \"test\")"
    sys.exit()

section = sys.argv[1]

#
# Read the configuration file
#
parser = SafeConfigParser()
parser.read("config.ini")

#
# Configure these constants
#
adminAuth = ("admin", parser.get(section, "adminPassword"))  # replace PASSWORD with admin password
targetUrl = parser.get(section, "targetUrl")         # change this according to your desired environment
portletPath = parser.get(section, "portletPath")     # change this according to which portlet you want to restart

#
# End the program early if response code is not 200
#
def check200(r):
    if r.status_code != 200:
        print "Error: Got back status code", r.status_code
        sys.exit()

#
# Main program execution begins here
#
if __name__ == "__main__":

    # create a new session so that cookies can be stored
    sesh = requests.session()

    # send a GET request to get the HTML source for the Tomcat Manager main page
    r = sesh.get(targetUrl, auth=adminAuth)
    check200(r)

    # pull the cross site request forgery key out of the HTML using a regex
    csrfKey = re.search(r'CSRF_NONCE=(.*)"', r.text).group(1)

    # send a POST request to the Tomcat Manager that reloads a portlet
    dataStuff = {
        "path": portletPath,
        "org.apache.catalina.filters.CSRF_NONCE": csrfKey
    }
    r2 = sesh.post(targetUrl + "/reload", data=dataStuff, auth=adminAuth)
    check200(r)

